-- --------------------------------------------------------
-- Hostitel:                     127.0.0.1
-- Verze serveru:                5.6.21 - MySQL Community Server (GPL)
-- OS serveru:                   Win32
-- HeidiSQL Verze:               9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportování struktury databáze pro
CREATE DATABASE IF NOT EXISTS `topporn` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_czech_ci */;
USE `topporn`;


-- Exportování struktury pro tabulka topporn.collection
CREATE TABLE IF NOT EXISTS `collection` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_czech_ci,
  `image_src` varchar(300) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.config
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL,
  `param` varchar(200) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `value` varchar(200) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_czech_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.diffs
CREATE TABLE IF NOT EXISTS `diffs` (
  `id` int(11) NOT NULL,
  `install_date` datetime DEFAULT NULL,
  `install_user` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `log` text COLLATE utf8mb4_czech_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL,
  `video_id` int(11) DEFAULT NULL,
  `src` varchar(300) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `thumb_src` varchar(300) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_czech_ci,
  PRIMARY KEY (`id`),
  KEY `gall on video` (`video_id`),
  CONSTRAINT `gall on video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_czech_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.tag_alias
CREATE TABLE IF NOT EXISTS `tag_alias` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alias on tag` (`tag_id`),
  CONSTRAINT `alias on tag` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `password` varchar(512) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `video_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.video
CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_title` varchar(300) COLLATE utf8mb4_czech_ci NOT NULL DEFAULT '0',
  `title` varchar(300) COLLATE utf8mb4_czech_ci NOT NULL DEFAULT '0',
  `collection_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_czech_ci,
  `length` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `released` tinyint(4) DEFAULT '1',
  `rating` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `video on user` (`author_id`),
  KEY `video on kolekce` (`collection_id`),
  CONSTRAINT `video on kolekce` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`id`) ON DELETE SET NULL,
  CONSTRAINT `video on user` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.video_options
CREATE TABLE IF NOT EXISTS `video_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `host` varchar(256) COLLATE utf8mb4_czech_ci NOT NULL DEFAULT '0',
  `embed` text COLLATE utf8mb4_czech_ci,
  PRIMARY KEY (`id`),
  KEY `videoOption on video` (`video_id`),
  CONSTRAINT `videoOption on video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro tabulka topporn.views
CREATE TABLE IF NOT EXISTS `views` (
  `id` int(11) NOT NULL,
  `video_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `views on video` (`video_id`),
  CONSTRAINT `views on video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- Export dat nebyl vybrán.


-- Exportování struktury pro trigger topporn.set_diff
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `set_diff` AFTER INSERT ON `diffs` FOR EACH ROW INSERT INTO diffs (id, install_date, install_user) VALUES (id, now(), user())
  ON DUPLICATE KEY UPDATE install_date = now(), install_user = user(), log = 'Diff byl aplikován znovu, pozor na duplicitní instalace!'//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
