<?php

namespace App\AdminModule\Presenters;

use Nette;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    public function startup()
    {
        parent::startup();
        $this->detectAccess();
    }

    private function detectAccess()
    {
        if ($this->name !== 'Admin:Sign') {
            if (!$this->getUser()->isLoggedIn()) {
                if ($this->getUser()->getLogoutReason() === Nette\Security\IUserStorage::INACTIVITY) {
                    $this->flashMessage('Byl jste automaticky odhlášen (20 minut).', 'info');
                } else {
                    $this->flashMessage('Přihlašte se prosím.', 'info');
                }
                $this->redirect('Sign:default');
            } else {
                if (!$this->getUser()->isAllowed('Admin:*')) {
                    $this->flashMessage('Přístup odmítnut.', 'danger');
                    $this->logout();
                    $this->redirect('Sign:default');
                }
            }
        } else {
            if ($this->getUser()->isLoggedIn()) {
                if ($this->getUser()->isInRole('admin')) {
                    $this->redirect('Homepage:default');
                }
            }
        }
    }

    public function handleLogout()
    {
        $this->logout();
        $this->flashMessage('Byl jste odhlášen.', 'info');
        $this->redirect('Sign:default');
    }

    public function logout()
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->getUser()->logout();
        }
    }

}
